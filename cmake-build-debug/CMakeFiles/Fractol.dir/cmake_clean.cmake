file(REMOVE_RECURSE
  "CMakeFiles/Fractol.dir/srcs/main.c.o"
  "CMakeFiles/Fractol.dir/srcs/window/new_window.c.o"
  "CMakeFiles/Fractol.dir/srcs/fractal/init_mandelbrot.c.o"
  "CMakeFiles/Fractol.dir/srcs/fractal/re_im_factor.c.o"
  "CMakeFiles/Fractol.dir/srcs/fractal/draw_mandelbrot.c.o"
  "CMakeFiles/Fractol.dir/srcs/window/set_pixel.c.o"
  "CMakeFiles/Fractol.dir/srcs/scene/control_system/key_control.c.o"
  "CMakeFiles/Fractol.dir/srcs/scene/new_scene.c.o"
  "CMakeFiles/Fractol.dir/srcs/scene/update_scene.c.o"
  "CMakeFiles/Fractol.dir/srcs/window/new_image_window.c.o"
  "CMakeFiles/Fractol.dir/srcs/fractal/init_julia.c.o"
  "CMakeFiles/Fractol.dir/srcs/fractal/draw_julia.c.o"
  "CMakeFiles/Fractol.dir/srcs/fractal/init_burning_ship.c.o"
  "CMakeFiles/Fractol.dir/srcs/fractal/draw_burning_ship.c.o"
  "CMakeFiles/Fractol.dir/srcs/window/init_color_set.c.o"
  "CMakeFiles/Fractol.dir/srcs/fractal/set_iteration_pixel.c.o"
  "CMakeFiles/Fractol.dir/srcs/scene/control_system/mouse_control.c.o"
  "CMakeFiles/Fractol.dir/srcs/messages/print_usage.c.o"
  "CMakeFiles/Fractol.dir/srcs/messages/ft_putstr.c.o"
  "Fractol.pdb"
  "Fractol"
)

# Per-language clean rules from dependency scanning.
foreach(lang C)
  include(CMakeFiles/Fractol.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
