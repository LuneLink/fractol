/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/08 09:14:57 by spetrenk          #+#    #+#             */
/*   Updated: 2017/03/08 09:15:02 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WINDOW_H
# define WINDOW_H

# include <stdlib.h>
# include <mlx.h>
# include <math.h>

typedef	struct 	s_color
{
	char 		red;
	char 		blue;
	char 		green;
}				t_color;


typedef struct	s_window
{
	void 		*p_mlx;
	void 		*p_win;
	void		*p_img;
	char 		*data;
	size_t 		height;
	size_t 		width;
	int 		bit_per_pixel;
	int 		size_line;
	int 		endian;
	t_color		*color_set;
	short		color_count;
}				t_window;

t_window		*new_window(size_t width, size_t height, char *name);
void			set_pixel(t_window *window, size_t x, size_t y, t_color *color);
void			new_image_window(t_window *window);
void			init_color_set(t_window *window, short count);

#endif
