//
// Created by Serhii Petrenko on 3/10/17.
//

#ifndef FRACTOL_MESSAGES_H
# define FRACTOL_MESSAGES_H

# include <unistd.h>

	void ft_putstr(char *str);
	void print_usage();
#endif
