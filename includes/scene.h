/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/10 11:29:20 by spetrenk          #+#    #+#             */
/*   Updated: 2017/03/10 11:29:23 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_SCENE_H
# define FRACTOL_SCENE_H

#include "fractal.h"

typedef struct	s_scene
{
	t_fractal	*fractal;
	t_window	*window;
	void		(*draw)(t_window *, t_fractal*);
	char 		is_right_pressed;
}				t_scene;

t_scene		*new_scene(char fract_symb, size_t win_width, size_t win_height);
int			key_control(int keycode, t_scene *scene);
int			mouse_press(int key, int x, int y, t_scene * scene);
int			mouse_release(int key, int x, int y, t_scene *scene);
int			mouse_move(int x, int y, t_scene *scene);
void		update_scene(t_scene *scene);

#endif
