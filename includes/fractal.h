/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractal.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/08 09:14:29 by spetrenk          #+#    #+#             */
/*   Updated: 2017/03/08 09:14:50 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTAL_H
# define FRACTAL_H

# include <stdlib.h>
# include <math.h>
# include "window.h"

typedef struct	s_fract
{
	double		min_re;
	double		max_re;
	double		min_im;
	double		max_im;
	double		re_factor;
	double		im_factor;
	double		z_re;
	double		c_re;
	double		z_im;
	double		c_im;
	short		max_iteration;
	short		color_gama;
}				t_fractal;


void			re_im_factor(t_fractal *fractal, size_t width, size_t height);
void			set_iteration_pixel(t_window *window, t_fractal *fractal,
									size_t iter, size_t *xy);
t_fractal		*init_mandelbrot();
t_fractal		*init_julia();
t_fractal		*init_burning_ship();

void			draw_mandelbrot(t_window *window, t_fractal *fractal);
void			draw_julia(t_window *window, t_fractal *fractal);
void			draw_burning_ship(t_window *window, t_fractal *fractal);

#endif
