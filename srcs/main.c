/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/10 11:30:31 by spetrenk          #+#    #+#             */
/*   Updated: 2017/03/10 11:30:34 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <mlx.h>
#include <fractal.h>
#include <scene.h>
#include <unistd.h>
#include "messages.h"

static void	run_process(char *symb)
{
	t_scene		*scene;

	if (symb[1] == 0)
	{
		scene = new_scene(symb[0], 800, 800);
		if (scene)
		{
			update_scene(scene);
			mlx_hook(scene->window->p_win, 2, 5, key_control, scene);
			mlx_hook(scene->window->p_win, 4, 0, mouse_press, scene);
			mlx_hook(scene->window->p_win, 5, 0, mouse_release, scene);
			mlx_hook(scene->window->p_win, 6, 0, mouse_move, scene);
			mlx_loop(scene->window->p_mlx);
			return;
		}
	}
	print_usage();
}

int main(int argc, char **argv)
{
	pid_t	pid[argc - 1];
	short	i;

	if (argc != 1)
	{
		i = -1;
		while(++i < argc)
		{
			pid[i] = fork();
			if (pid[i] == 0)
				run_process(argv[i + 1]);
		}
	}
	else
		print_usage();
	return (0);
}
