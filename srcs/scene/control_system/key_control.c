//
// Created by Serhii Petrenko on 3/8/17.
//

#include "scene.h"

static char	add_iterations(t_fractal *fractal, char i)
{
	if (i < 0 && fractal->max_iteration != 0)
	{
		fractal->max_iteration--;
		return (1);
	}
	if (i > 0 && fractal->max_iteration < 600)
	{
		fractal->max_iteration++;
		return (1);
	}
	return (0);
}

static char	gama_incr(t_fractal *fractal, char i)
{
	if (i < 0 && fractal->max_iteration != 0)
		fractal->color_gama--;
	else
		fractal->color_gama++;
	return (1);
}

int	key_control(int keycode, t_scene *scene)
{
	char redraw;

	redraw = 0;
	//printf("%d\n", keycode);
	if (keycode == 53)
		exit(0);
	else if (keycode == 69)
		redraw = add_iterations(scene->fractal, 1);
	else if (keycode == 78)
		redraw = add_iterations(scene->fractal, -1);
	else if (keycode == 67)
		redraw = gama_incr(scene->fractal, 1);
	else if (keycode == 75)
		redraw = gama_incr(scene->fractal, -1);
	if (redraw)
		update_scene(scene);
	return (0);
}