//
// Created by Serhii Petrenko on 3/9/17.
//

#include "scene.h"

static char	zoom(t_scene *scene, double x, double y, double mult)
{
	t_fractal	*f;
	double		x_mult;
	double		y_mult;

	f = scene->fractal;
	x = f->min_re + f->re_factor * x;
	y = f->min_im + f->im_factor * y;
	x_mult = x * (1 - mult);
	y_mult = y * (1 - mult);
	f->max_re = f->max_re * mult + x_mult;
	f->min_re = f->min_re * mult + x_mult;
	f->max_im = f->max_im * mult + y_mult;
	f->min_im = f->min_im * mult + y_mult;
	return (1);
}

int	mouse_press(int key, int x, int y, t_scene *scene)
{
	char redraw;

	redraw = 0;
	if (key == 4)
		redraw = zoom(scene, x, y, 1.1);
	else if (key == 5)
		redraw = zoom(scene, x, y, 0.9);
	else if (key == 1)
		scene->is_right_pressed = 1;
	if (redraw)
		update_scene(scene);
	return (0);
}

int	mouse_release(int key, int x, int y, t_scene *scene)
{
	if (key == 1)
		scene->is_right_pressed = 0;
}

int	mouse_move(int x, int y, t_scene *scene)
{
	if (scene->is_right_pressed && (x > 0 && x < scene->window->width) &&
			(y > 0 && y < scene->window->height))
	{
		scene->fractal->c_re = -((double)x / scene->window->width);
		scene->fractal->c_im = ((double)y / scene->window->height);
		update_scene(scene);
	}
	return (0);
}