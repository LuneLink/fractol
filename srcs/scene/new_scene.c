//
// Created by Serhii Petrenko on 3/8/17.
//

#include "scene.h"

static t_scene	*init_scene(char *scene_name, size_t w, size_t h,
									t_fractal *(*init)(),
							  void (*draw)(t_window *, t_fractal *))
{
	t_scene	*scene;

	scene = (t_scene*)malloc(sizeof(t_scene));
	scene->fractal = init();
	scene->draw = draw;
	scene->window = new_window(w, h, scene_name);
	scene->is_right_pressed = 0;
	return (scene);
}

t_scene			*new_scene(char fract_symb, size_t win_width, size_t win_height)
{
	if (fract_symb == 'M')
		return (init_scene("Mandelbrot",win_width, win_height,
						   init_mandelbrot, draw_mandelbrot));
	else if (fract_symb == 'J')
		return (init_scene("Julia",win_width, win_height,
						   init_julia, draw_julia));
	else if (fract_symb == 'S')
		return (init_scene("Burning Ship",win_width, win_height,
						   init_burning_ship, draw_burning_ship));
	return (0);
}