//
// Created by Serhii Petrenko on 3/8/17.
//

#include "scene.h"

void		update_scene(t_scene *scene)
{
	new_image_window(scene->window);
	scene->draw(scene->window, scene->fractal);
	mlx_put_image_to_window(scene->window->p_mlx, scene->window->p_win,
							scene->window->p_img, 0, 0);
}