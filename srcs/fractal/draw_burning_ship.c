//
// Created by Serhii Petrenko on 3/9/17.
//

#include "fractal.h"

static void	c_re_calculate(t_fractal *fractal, size_t x)
{
	fractal->c_re = fractal->min_re + x * fractal->re_factor;
	fractal->z_re = 0;
	fractal->z_im = 0;
}

static size_t	iteration(t_fractal *fractal)
{
	size_t	iter;
	double	z_im2;

	iter = -1;
	while (++iter < fractal->max_iteration)
	{
		if (fabs(fractal->z_im * fractal->z_im +
						 fractal->z_re * fractal->z_re) > 4)
			return (iter);
		z_im2 = 2 * fabs(fractal->z_re * fractal->z_im) + fractal->c_im;
		fractal->z_re = fractal->z_re * fractal->z_re - fractal->z_im*
										fractal->z_im + fractal->c_re;
		fractal->z_im = z_im2;
	}
	return (iter);
}

void			draw_burning_ship(t_window *window, t_fractal *fractal)
{
	size_t	xy[2];
	size_t	iter;

	re_im_factor(fractal, window->width, window->height);
	xy[1] = -1;
	while (++xy[1] < window->height)
	{
		fractal->c_im = fractal->min_im + xy[1]  * fractal->im_factor;
		xy[0]  = -1;
		while (++xy[0] < window->width)
		{
			c_re_calculate(fractal, xy[0]);
			if ((iter = iteration(fractal)) != fractal->max_iteration)
				set_iteration_pixel(window, fractal, iter, xy);
		}
	}
}