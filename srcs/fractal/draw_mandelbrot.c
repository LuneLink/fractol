//
// Created by Serhii Petrenko on 3/8/17.
//

#include "fractal.h"

static void	c_re_calculate(t_fractal *fractal, size_t x)
{
	fractal->c_re = fractal->min_re + x * fractal->re_factor;
	fractal->z_re = fractal->c_re;
	fractal->z_im = fractal->c_im;
}

static size_t iteration(t_fractal *fractal)
{
	size_t	iter;
	double	z_re2;
	double	z_im2;

	iter = -1;
	while (++iter < fractal->max_iteration)
	{
		z_re2 = fractal->z_re * fractal->z_re;
		z_im2 = fractal->z_im * fractal->z_im;
		if (z_re2 + z_im2 > 4)
			return (iter);
		fractal->z_im = 2 * fractal->z_re * fractal->z_im + fractal->c_im;
		fractal->z_re = z_re2 - z_im2 + fractal->c_re;
	}
	return (iter);
}

void	draw_mandelbrot(t_window *window, t_fractal *fractal)
{
	size_t	xy[2];
	size_t	iter;

	re_im_factor(fractal, window->width, window->height);
	xy[1] = -1;
	while (++xy[1] < window->height)
	{
		fractal->c_im = fractal->min_im + xy[1] * fractal->im_factor;
		xy[0] = -1;
		while (++xy[0] < window->width)
		{
			c_re_calculate(fractal, xy[0]);
			if ((iter = iteration(fractal)) != fractal->max_iteration)
				set_iteration_pixel(window, fractal, iter, xy);
		}
	}
}