//
// Created by Serhii Petrenko on 3/9/17.
//

#include "fractal.h"

t_fractal		*init_burning_ship()
{
	t_fractal *fractal;

	fractal = (t_fractal *)malloc(sizeof(t_fractal));
	fractal->min_im = -1.5;
	fractal->max_im = 1.5;
	fractal->min_re = -2;
	fractal->max_re = 1;
	fractal->c_im = 0.0;
	fractal->c_re = 0.0;
	fractal->max_iteration = 100;
	return (fractal);
}
