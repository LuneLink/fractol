//
// Created by Serhii Petrenko on 3/7/17.
//

#include "fractal.h"

t_fractal		*init_mandelbrot()
{
	t_fractal *fractal;

	fractal = (t_fractal *)malloc(sizeof(t_fractal));
	fractal->min_im = -1.2;
	fractal->max_im = 1.2;
	fractal->min_re = -2.0;
	fractal->max_re = 1.0;
	fractal->max_iteration = 100;
	fractal->color_gama = 0;
	return (fractal);
}
