//
// Created by Serhii Petrenko on 3/8/17.
//

#include "fractal.h"

t_fractal		*init_julia()
{
	t_fractal *fractal;

	fractal = (t_fractal *)malloc(sizeof(t_fractal));
	fractal->min_im = -1.4;
	fractal->max_im = 1.4;
	fractal->min_re = -1.4;
	fractal->max_re = 1.4;
	fractal->c_im = 0.27015;
	fractal->c_re = -0.7;
	fractal->max_iteration = 100;
	return (fractal);
}
