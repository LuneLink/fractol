//
// Created by Serhii Petrenko on 3/7/17.
//

#include "fractal.h"

void			re_im_factor(t_fractal *fractal, size_t width, size_t height)
{
	fractal->re_factor = (fractal->max_re - fractal->min_re) / (width - 1);
	fractal->im_factor = (fractal->max_im - fractal->min_im) / (height - 1);
}