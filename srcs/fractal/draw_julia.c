//
// Created by Serhii Petrenko on 3/8/17.
//

#include "fractal.h"

static void		c_reim_calculate(t_fractal *fractal, size_t x, size_t y, t_window *window)
{
	fractal->z_re = x * (fractal->max_re - fractal->min_re)/
					window->width + fractal->min_re;
	fractal->z_im = y * (fractal->max_re - fractal->min_re)/
					window->height + fractal->min_im;
}

static size_t	iteration(t_fractal *fractal)
{
	size_t	iter;
	double	z_re2;
	double	z_im2;

	iter = -1;
	while (++iter < fractal->max_iteration)
	{
		if (fabs(fractal->z_im * fractal->z_im + fractal->z_re * fractal->z_re) > 4)
			break;
		z_re2 = fractal->z_re;
		z_im2 = fractal->z_im;
		fractal->z_re = z_re2 * z_re2 - z_im2 * z_im2 + fractal->c_re;
		fractal->z_im = 2.0 * z_re2 * z_im2 + fractal->c_im;
	}
	return (iter);
}


// ned optimize when black color
void	draw_julia(t_window *window, t_fractal *fractal)
{
	size_t	xy[2];
	size_t 	iter;

	re_im_factor(fractal, window->width, window->height);
	xy[1] = -1;
	while(++xy[1] < window->height)
	{
		xy[0] = -1;
		while (++xy[0]< window->width)
		{
			c_reim_calculate(fractal, xy[0], xy[1], window);
			if ((iter = iteration(fractal)) != fractal->max_iteration)
				set_iteration_pixel(window, fractal, iter, xy);
		}
	}
}
