//
// Created by Serhii Petrenko on 3/9/17.
//

#include "fractal.h"

void	set_iteration_pixel(t_window *window, t_fractal *fractal, size_t iter, size_t *xy)
{
	set_pixel(window, xy[0], xy[1], &(window->color_set[(iter +
			fractal->color_gama) % window->color_count]));
}
