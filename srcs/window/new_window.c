//
// Created by Serhii Petrenko on 3/7/17.
//

#include "window.h"

t_window		*new_window(size_t width, size_t height, char *name)
{
	t_window *window;

	window = (t_window *)malloc(sizeof(t_window));
	window->p_mlx = mlx_init();
	window->p_win = mlx_new_window(window->p_mlx, (int)width, (int)height, name);
	window->p_img = 0;
	new_image_window(window);
	window->height = height;
	window->width = width;
	window->color_set = 0;
	init_color_set(window, 255);
	return (window);
}
