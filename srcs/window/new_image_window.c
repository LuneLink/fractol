//
// Created by Serhii Petrenko on 3/8/17.
//

#include "window.h"

void			new_image_window(t_window *window)
{
	if (window->p_img)
		mlx_destroy_image(window->p_mlx, window->p_img);
	window->p_img = mlx_new_image(window->p_mlx,
								  (int)window->width, (int)window->height);
	window->data = mlx_get_data_addr(window->p_img, &(window->bit_per_pixel),
									 &(window->size_line), &(window->endian));
}