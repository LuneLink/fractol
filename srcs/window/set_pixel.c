//
// Created by Serhii Petrenko on 3/8/17.
//

#include "scene.h"

void	set_pixel(t_window *window, size_t x, size_t y, t_color *color)
{
	char	*data;
	size_t	pos;

	data = window->data;
	pos = y * window->width + x;
	if (!color)
	{
//		data[pos * 4] = (char) 0;
//		data[pos * 4 + 1] = (char) 0;
//		data[pos * 4 + 2] = (char) 0;
	}
	else
	{
		data[pos * 4] = color->red;
		data[pos * 4 + 1] = color->green;
		data[pos * 4 + 2] = color->blue;
	}
}