//
// Created by Serhii Petrenko on 3/9/17.
//

#include "window.h"

void	init_color_set(t_window *window, short count)
{
	short i;
	float f;

	free(window->color_set);
	window->color_set = (t_color *)malloc(sizeof(t_color) * count);
	i = -1;
	f = 0;
	while (++i < count)
	{
		window->color_set[i].red = (unsigned char)((-cos(f) + 1) * 127.0f);
		window->color_set[i].green = (unsigned char)((sin(f) + 1) * 127.0f);
		window->color_set[i].blue = (unsigned char)((cos(f) + 1) * 127.0f);
		f += 2 * M_PI / count;
	}
	window->color_count = count;
}