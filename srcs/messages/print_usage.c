//
// Created by Serhii Petrenko on 3/10/17.
//

#include "messages.h"

void print_usage()
{
	ft_putstr("Usage: ./fractol <fractol symb> <fractol symb>...\n");
	ft_putstr("fractol symb:\n");
	ft_putstr("M - Mandelbrot\n");
	ft_putstr("J - Julia\n");
	ft_putstr("S - Burning Ship\n");
}