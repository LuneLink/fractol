//
// Created by Serhii Petrenko on 3/10/17.
//

#include "messages.h"

void  ft_putstr(char *str)
{
	while (*str)
	{
		write(1, str, 1);
		str++;
	}
}